unit USocisMdl;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Datasnap.DBClient, FMX.ListBox;

type
  TSocisMdl = class(TDataModule)
    cdsResult: TClientDataSet;
  private
  public
    function GetSocisName(ActCodes: string): Boolean;
    function GetEventsSoci(ActCodes, Any: string): Boolean;

    class function DesvinculaSoci(ActCodes, Id: string): Boolean;
    class procedure AddUsersToCombobox(ActCodes: string; cbSoci: TComboBox);
  end;

var
  SocisMdl: TSocisMdl;

implementation

uses
  System.JSON,
  URESTMdl, UClasses;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TSocisMdl }

class procedure TSocisMdl.AddUsersToCombobox(ActCodes: string;
  cbSoci: TComboBox);
var
  L: TStringList;
  Mdl: TSocisMdl;
begin
  cbSoci.Clear;

  L := nil;
  Mdl := nil;
  try
    L := TStringList.Create;
    Mdl := TSocisMdl.Create(nil);

    L.CommaText := ActCodes;
    Mdl.GetSocisName(L.CommaText);
    if Mdl.cdsResult.Active then
      TThread.Synchronize(TThread.CurrentThread,
        procedure
        var
          i: Integer;
          Soci: TSoci;
        begin
          for i := 0 to L.Count - 1 do
          begin
            if Mdl.cdsResult.Locate('actcode', L[i], []) then
            begin
              Soci := TSoci.Create;
              Soci.ActCode := Mdl.cdsResult.FieldByName('actcode').AsString;
              Soci.Nom := Mdl.cdsResult.FieldByName('nom').AsString;
              cbSoci.Items.AddObject(Mdl.cdsResult.FieldByName('nom').AsString, Soci)
            end;
          end;

          if cbSoci.Count > 0 then
            cbSoci.ItemIndex := 0;
        end);
  finally
    FreeAndNil(Mdl);
    FreeAndNil(L);
  end;
end;

class function TSocisMdl.DesvinculaSoci(ActCodes, Id: string): Boolean;
var
  Mdl: TSocisMdl;
  Obj: TJSONObject;
begin
  Mdl := TSocisMdl.Create(nil);
  try
    Obj := TJSONObject.Create;
    try
      Obj.AddPair('func', TJSONString.Create('DesvinculaSoci'));
      Obj.AddPair('actcode', TJSONString.Create(ActCodes));
      Obj.AddPair('id', TJSONString.Create(Id));

      TRESTMdl.GetRESTResponse('', Mdl.cdsResult, Obj);
    finally
      FreeAndNil(Obj);
    end;

    Result := False;
    if Mdl.cdsResult.Active then
      if Mdl.cdsResult.RecordCount > 0 then
        if SameStr(Mdl.cdsResult.FieldByName('status').AsString, 'ok') then
          Result := True;
  finally
    FreeAndNil(Mdl);
  end;
end;

function TSocisMdl.GetEventsSoci(ActCodes, Any: string): Boolean;
var
  Obj: TJSONObject;
begin
  Obj := TJSONObject.Create;
  try
    Obj.AddPair('func', TJSONString.Create('GetAssistencia'));
    Obj.AddPair('actcode', TJSONString.Create(ActCodes));
    Obj.AddPair('any', TJSONString.Create(Any));

    TRESTMdl.GetRESTResponse('', cdsResult, Obj);
  finally
    FreeAndNil(Obj);
  end;

  Result := cdsResult.Active and (cdsResult.RecordCount > 0);
end;

function TSocisMdl.GetSocisName(ActCodes: string): Boolean;
var
  Obj: TJSONObject;
begin
  Obj := TJSONObject.Create;
  try
    Obj.AddPair('func', TJSONString.Create('GetSociName'));
    Obj.AddPair('actcode', TJSONString.Create(ActCodes));

    TRESTMdl.GetRESTResponse('', cdsResult, Obj);
  finally
    FreeAndNil(Obj);
  end;

  Result := cdsResult.Active and (cdsResult.RecordCount > 0);
end;

end.
