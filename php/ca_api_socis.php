<?php
  /*******************************************************************************************************************************************************************
    Funció que afegeix un dispositiu.
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $params => paràmetres rebuts per url
                   * uuid: identificador únic per al dispositiu (GUID o data-hora en format yyyymmddhhnnss)
                   * so: sistema operatiu del dispositiu
  *******************************************************************************************************************************************************************/
  function AddDispositiu($bd_connection, $params) {
    $params['uuid'] = NormalizeParam($params['uuid']);
    $params['so'] = NormalizeParam($params['so']);

    $sql = 'select * from '.db_prefix.'dispositius where uuid = \''.$params['uuid'].'\'';
    $res = get_sql($bd_connection, $sql, false, false);
    
    $rows = $bd_connection->numRows($res);

    if ($rows != 0) {
      get_sql($bd_connection, $sql, false, true);
    } 
    else {
      // afegim registre
      $sql = 'insert into '.db_prefix.'dispositius (uuid, so, alta) values (\''.$params['uuid'].'\', \''.$params['so'].'\', \''.date("Y-m-d").'\')';
      get_sql($bd_connection, $sql, false, true);
    }
  }
 
  /*******************************************************************************************************************************************************************
    Funció que genera el Activation Code agafant aleatòriament 8 dígits alfanumèrics. Comprova que no existeixi a la taula ca_disp_socis
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $length => longitud de la cadena a retornar. Per defecte 8
    Retorna: string amb el Activation Code
  *******************************************************************************************************************************************************************/
  function generateRandomString($bd_connection, $length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    
    do {
      $exit = false;
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
         $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      $sql = 'select * from '.db_prefix.'disp_socis where actcode = \''.$randomString.'\'';
      $res = get_sql($bd_connection, $sql, false, false);
      $exit = $bd_connection->numRows($res) == 0;
    } while (!$exit);
    
    return $randomString;
  }
  
  /*******************************************************************************************************************************************************************
    Funció que retorna les temporades associats a la APP. 
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $params => paràmetres rebuts per url
                   * id: identificador del dispositiu associat a l'usuari
                   * mail: correu electrònic del soci
                   * mobil: mòbil del soci
                   * data_naix: data de naixement del soci
  *******************************************************************************************************************************************************************/
  function GetUserCode($bd_connection, $params) {
    // mirem si existeix el soci amb les dades introduïdes en la pantalla
    $sql = 'select * from '.db_prefix.'socis where data_naix = \''.$params['data_naix'].'\' and mobil = \''.$params['mobil'].'\' and mail = \''.$params['mail'].'\'';
    $res = get_sql($bd_connection, $sql, false, false);
    $rows = $bd_connection->numRows($res);

    if ($rows != 0) { // existeix, tenim 2 opcions, que ja s'hagi identificat (reenviar codi), que no ho hagi fet (crear codi i enviar)
      // agafem info registre
      $row = $res->fetch_assoc();
      
      // mirem si existeix relació dispositiu-soci
      $sql = 'select * from '.db_prefix.'disp_socis where id_disp = '.$params['id'].' and id_soci = '.$row['id'];
      $res = get_sql($bd_connection, $sql, false, false);
      $rows = $bd_connection->numRows($res);
      
      if ($rows != 0) { // ja estava d'alta, reenviem Activation Code
        $row1 = $res->fetch_assoc();
        $code = $row1['actcode'];
      }
      else { // no estava donat d'alta, creem nou Activation Code i l'afegim
        $code = generateRandomString($bd_connection, 8);
        $sql = 'insert into '.db_prefix.'disp_socis (id_disp, id_soci, actcode) values ('.$params['id'].', '.$row['id'].', \''.$code.'\')';
        get_sql($bd_connection, $sql, false, false);
      }
      
      // enviem correu 
      $missatge = "Registre del programa CdAPP dels Castellers d'Andorra\n\n"; 
      $missatge .= "Aquestes són les teves dades de registre:\n"; 
      $missatge .= "Correu electronic: ".$params['mail']."\n"; 
      $missatge .= "Telèfon de contacte: ".$params['mobil']."\n"; 
      $missatge .= "Data de Naixement: ".$params['data_naix']."\n\n"; 
      $missatge .= "Per poder entrar a la CdAPP hauràs d'introduir el següent codi:\n"; 
      $missatge .= "Codi: ".$code."\n\n\n"; 
      $missatge .= "Moltes gràcies per registrar-te de part de tot l'equip tècnic i directiu, i esperem que gaudeixis de l'aplicació.\n\n"; 
      $missatge .= "Ens veiem a l'assaig!"; 
      
      $assumpte = "Activació del compte a CdAPP"; 
        
      $headers = "From : ".webmaster_email;
        
      if (mail($params['mail'], $assumpte, $missatge, $headers) == false) {
        return_emty_json($bd_connection, '', true);
      } 
      else {
        $sql = 'select actcode from '.db_prefix.'disp_socis where id_disp = '.$params['id'].' and id_soci = '.$row['id'];  
        get_sql($bd_connection, $sql, false, true);
      }
    } 
    else { // no existeix, retornar error
      return_emty_json($bd_connection, '', true);
    }
  }
  
  /*******************************************************************************************************************************************************************
    Funció que retorna els rols del soci identificat en la APP. 
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $params => paràmetres rebuts per url
                   * actcode: codi d'activació relacionat amb l'usuari
  *******************************************************************************************************************************************************************/
  function GetRols($bd_connection, $params) {
    $sql  = 'select d.id_soci, r.id_rol ';
    $sql .= 'from '.db_prefix.'disp_socis d ';
    $sql .= '  inner join '.db_prefix.'rols_socis r on r.id_soci = d.id_soci ';
    $sql .= 'where d.actcode = \''.$params['actcode'].'\'';
	$res = get_sql($bd_connection, $sql, false, true);
  }
  
  /*******************************************************************************************************************************************************************
    Funció que retorna informació sobre un soci. 
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $params => paràmetres rebuts per url
                   * actcode: codi d'activació relacionat amb l'usuari
  *******************************************************************************************************************************************************************/
  function GetSociInfo($bd_connection, $params) {
    $sql  = 'select s.*, r.id_rol ';
    $sql .= 'from '.db_prefix.'disp_socis d ';
    $sql .= '  inner join '.db_prefix.'socis s on s.id = d.id_soci ';
    $sql .= '  inner join '.db_prefix.'rols_socis r on r.id_soci = d.id_soci ';
    $sql .= 'where d.actcode = \''.$params['actcode'].'\'';
	$res = get_sql($bd_connection, $sql, false, true);
  }
  
  /*******************************************************************************************************************************************************************
    Funció que retorna el nom d'un o més socis. 
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $params => paràmetres rebuts per url
                   * actcode: codis d'activació dels socis (separats per ,)
  *******************************************************************************************************************************************************************/
  function GetSociName($bd_connection, $params) {
    $params['actcode'] = explode(',', $params['actcode']);
    $where = '';
    foreach ($params['actcode'] as $valor) {
      if ($where != '') $where .= ',';
      $where .= '\''.$valor.'\'';
    }
    
    $sql  = 'select d.actcode, s.nom ';
    $sql .= 'from '.db_prefix.'disp_socis d ';
    $sql .= '  inner join '.db_prefix.'socis s on s.id = d.id_soci ';
    $sql .= 'where d.actcode in ('.$where.')';
	$res = get_sql($bd_connection, $sql, false, true);
  }
  
  /*******************************************************************************************************************************************************************
    Funció que desvincula un soci d'un dispositiu.
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $params => paràmetres rebuts per url
                   * actcode: codi d'activació del soci a desvincular
                   * id: identificador del dispositiu desvincular de l'usuari
  *******************************************************************************************************************************************************************/
  function DesvinculaSoci($bd_connection, $params) {
    $sql = 'delete from '.db_prefix.'disp_socis where id_disp = '.$params['id'].' and actcode = \''.$params['actcode'].'\'';
    get_sql($bd_connection, $sql, false, false);
    
    return_emty_json($bd_connection, 'ok', true);
  }
  
  /*******************************************************************************************************************************************************************
    Funció que retorna els events associats al casteller.
    Paràmetres:
      - $bd_connection => objecte de connexió a la base de dades
      - $params => paràmetres rebuts per url
                   * actcode: codi d'activació del soci 
                   * any: any dels events 
  *******************************************************************************************************************************************************************/
  function GetAssistencia($bd_connection, $params) {
    $sql  = 'select a.* ';
    $sql .= 'from '.db_prefix.'disp_socis d ';
    $sql .= 'inner join '.db_prefix.'assistencia a on a.id_soci = d.id_soci ';
    $sql .= '  inner join '.db_prefix.'events e on e.id = a.id_event ';
    $sql .= 'where d.actcode = \''.$params['actcode'].'\' ';
    $sql .= '  and e.any = '.$params['any'];
  
    $res = get_sql($bd_connection, $sql, false, true);
  }
?>