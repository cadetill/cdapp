unit UAssistenciaFrm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  UBaseFrm, FMX.Layouts, FMX.Controls.Presentation, FMX.Objects, FMX.ListBox,
  UCalFrm, UInterfaces;

const
  cCaption = 'Gesti� Assist�ncia';

type
  TAssistenciaFrm = class(TBaseFrm, IChildren)
    pIcons: TPanel;
    imgBus: TImage;
    imgOk: TImage;
    imgNone: TImage;
    imgNo: TImage;
    imgCar: TImage;
    cbSoci: TComboBox;
    pSoci: TPanel;
    pData: TPanel;
    frData: TCalFrm;
    pCastells: TPanel;
    lCastells: TLabel;
    recColles: TCalloutRectangle;
    Image1: TImage;
    lbCastells: TListBox;
  private
  public
    constructor Create(AOwner: TComponent); override;

    function SetCaption: string;
    function DeactivateAll: Boolean;
    function AcceptForm: Boolean;
    function EnabledBackButton: Boolean;
    function DefaultStateAcceptButton: Boolean;
    procedure AfterShow;
  end;

var
  AssistenciaFrm: TAssistenciaFrm;

implementation

uses
  UClasses, UIniFiles, USocisMdl;

{$R *.fmx}

{ TAssistenciaFrm }

function TAssistenciaFrm.AcceptForm: Boolean;
begin

end;

procedure TAssistenciaFrm.AfterShow;
var
  L: TStringList;
begin
  Ani(True);
  TThread.CreateAnonymousThread(procedure
    begin
      // afegim usuaris
      TFileIni.SetFileIni(TGenFunc.GetIniName);
      L := nil;
      try
        L := TStringList.Create;
        TFileIni.GetSection('USERS', L);
        TSocisMdl.AddUsersToCombobox(L.CommaText, cbSoci);
      finally
        FreeAndNil(L);
      end;

      // treiem animaci�
      TThread.Synchronize(TThread.CurrentThread,
        procedure
        begin
          Ani(False);
        end);
    end).Start;

  if not Assigned(TagObject) then Exit;
  if not (TagObject is TItemEvent) then Exit;

  frData.Id := TItemEvent(TagObject).Id;

  frData.lType.Text := TItemEvent(TagObject).Nom_curt + ':';
  frData.lDesc.Text := TItemEvent(TagObject).Descrip;

  if TItemEvent(TagObject).Datai <> '' then
    frData.lCalendar.Text := FormatDateTime('dd/mm/yyyy', TItemEvent(TagObject).DataiToDate);
  if TItemEvent(TagObject).Busd <> '' then
    frData.lBus.Text := FormatDateTime('dd/mm/yyyy', TItemEvent(TagObject).BusdToDate);
  if TItemEvent(TagObject).Buslloc <> '' then
    if frData.lBus.Text <> '' then
      frData.lBus.Text := frData.lBus.Text + ' - ' + TItemEvent(TagObject).Buslloc
    else
      frData.lBus.Text := TItemEvent(TagObject).Buslloc;
  frData.lMeeting.Text := TItemEvent(TagObject).Quedar;
  frData.lPlace.Text := TItemEvent(TagObject).Lloc;
  if (TItemEvent(TagObject).Horai <> '') and (TItemEvent(TagObject).Horai <> '00:00:00') then
    frData.lTime.Text := FormatDateTime('hh''h''nn', TItemEvent(TagObject).HoraiToTime);
  frData.lColles.Text := TItemEvent(TagObject).Colles;

  gplLayout.RowCollection.Items[2].Value := frData.GetTotalHeight;
end;

constructor TAssistenciaFrm.Create(AOwner: TComponent);
begin
  inherited;

  lbCastells.Clear;
end;

function TAssistenciaFrm.DeactivateAll: Boolean;
begin
  Result := True;
end;

function TAssistenciaFrm.DefaultStateAcceptButton: Boolean;
begin
  Result := True;
end;

function TAssistenciaFrm.EnabledBackButton: Boolean;
begin
  Result := False;
end;

function TAssistenciaFrm.SetCaption: string;
begin
  Result := cCaption;
end;

end.
